#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <memory.h>
#include "unistd.h"

int id;
int nstr;
int nstr_counter = 0;
int master = 0;
char* filename = "out_file.txt";
size_t mem_size = sizeof(int) * 3;
int proc_number = 3;
int first_iter = 1;
int end = 0;

void write_mem(void* mem_ptr, int* data) {
  printf("%d: write in shmem, data = %d, %d, %d\n", id, data[0], data[1], data[2]);
  memcpy(mem_ptr, data, mem_size);
}

int* read_mem(void* mem_ptr) {
  printf("%d: read from shmem\n", id);
  int* res = (int*)malloc(mem_size);
  memcpy(res, mem_ptr, mem_size);
  return res;
}

int get_shmem(int key) {
  printf("%d: get shmem\n", id);
  int shmem_id = shmget(key, mem_size, IPC_EXCL);
  if (shmem_id == -1) {
    shmem_id = shmget(key, mem_size, IPC_CREAT | 0400 | 0200);
    master = 1;
    if(shmem_id == -1) {
      printf("%d: error creating shared memory\n", id);
      abort();
    }
  }
  return shmem_id;
}

void work(void *mem_ptr) {
  printf("%d: write file\n", id);
  FILE* out_file = fopen(filename, "a+");
  fprintf(out_file, "Process%d: pid: %d \n", id, getpid());
  if(id == 3) fprintf(out_file, "\n");
  fclose(out_file);
  sleep(1);
}

void update_control_values(void *mem_ptr, int cur_writer, int max_lines_count, int finished_pss) {
  printf("%d: update control values\n", id);
  int new_writer;
  int new_mlc = max_lines_count;
  int new_fp = finished_pss;

  if(first_iter) {
    if(max_lines_count < nstr)
      new_mlc = nstr;
    first_iter = 0;
  }

  nstr_counter++;

  if(nstr == nstr_counter && !end){//process is ready to finish
    new_fp++;
    end = 1;
  }

  if(id == 3){
    new_writer = 1;
    new_mlc--;
  }
  else {
    new_writer = cur_writer + 1;
  }

  int* new_data = (int*)malloc(mem_size);
  new_data[0] = new_writer;
  new_data[1] = new_mlc;
  new_data[2] = new_fp;
  printf("%d: new control values = {%d, %d, %d}\n", id, new_writer, new_mlc, new_fp);
  write_mem(mem_ptr, new_data);
}

void wait_file(void* mem_ptr) {
  int* control_data;
  int cur_writer;
  int max_lines_count;
  int finished_pss;

  do {
    control_data = read_mem(mem_ptr);
    cur_writer = control_data[0];
    max_lines_count= control_data[1];
    finished_pss = control_data[2];
    printf("%d: wait...\n", id);
    sleep(1);
  } while(cur_writer != id);
  printf("%d: stop wait\n", id);
  if(!end) work(mem_ptr);

  update_control_values(mem_ptr, cur_writer, max_lines_count, finished_pss);
}


int main(int argc, char* argv[]) {
  if (argc < 3){
    printf("usage: ./nmain id nstr\n");
    exit(1);
  }
  id = atoi(argv[1]);
  nstr = atoi(argv[2]);
  printf("Program id = %d\n", id);
  int key = 95;

  int shmem_id = get_shmem(key);
  printf("%d: master = %d\n", id, master);
  void* shm_ptr = shmat(shmem_id, 0, 0);

  if(master){
    int inital_data[] = {1, 0, 0};//current reader, max lines count(decreases after every iteration), finished process count
    write_mem(shm_ptr, inital_data);
  }

  int* control_data = NULL;
  int max_lines_count;
  int finished_pss;

  do{
    wait_file(shm_ptr);
    control_data = read_mem(shm_ptr);
    max_lines_count = control_data[1];
  }while(max_lines_count != 0);

  if(master) {
    do{
      control_data = read_mem(shm_ptr);
      finished_pss = control_data[2];
      sleep(1);
    }while(finished_pss < 3);
    printf("%d: master delete shmem", id);
    shmdt(shm_ptr);
    shmctl(shmem_id, IPC_RMID, NULL);
  }
  else shmdt(shm_ptr);

  return 0;
}
