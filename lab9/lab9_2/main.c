#include "unistd.h"
#include "stdio.h"
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdlib.h>
#include <memory.h>

int id;
void* mymem_ptr = NULL;
void* othmem_ptr = NULL;
int size = 4;
int shm_size;
int shm_size_byte;

void print_arr(int* arr, int size) {
  printf("%d: data = ", id);
  for (int i = 0; i < size; ++i)
    printf("%d, ", arr[i]);
  printf("\n");
}

void get_shm(int *mymem_id, int *othmem_id, int key1, int key2) {
  *mymem_id = shmget(key1, (size_t) shm_size_byte, IPC_CREAT | 0666);
  while(*othmem_id == -1) {
    *othmem_id = shmget(key2, (size_t) shm_size_byte, IPC_EXCL);
  }
  mymem_ptr = shmat(*mymem_id, NULL, 0);
  othmem_ptr = shmat(*othmem_id, NULL, 0);
}

int* read_data_from_shm(void* mem_ptr) {
  int data_size = 0;
  int* mem_ptr_int = (int*) mem_ptr;
  do {
    data_size = mem_ptr_int[0];
  }while(data_size <= 0);
  
  int* arr = (int*) malloc((size_t) shm_size_byte);
  arr[0] = data_size;
  for(int i = 1; i < data_size + 1; i++) {
    arr[i] = mem_ptr_int[i];
    if(id == 2) arr[i] = arr[i] - 1;
  }
  return arr;
}

int* init_arr() {
  int* arr = (int*) malloc((size_t) shm_size_byte);
  arr[0] = 0;//reserved for size
  arr[1] = 1;
  arr[2] = 2;
  arr[3] = 3;
  arr[4] = 4;
  return arr;
}

int main(int argc, char* argv[]){
  if (argc < 2){
    printf("usage: ./main id\n");
  }
  id = atoi(argv[1]);
  printf("Program id = %d\n", id);
  int mem1_id = -1;
  int mem2_id = -1;
  int key1 = 80;
  int key2 = 81;
  shm_size = size + 1;
  shm_size_byte = sizeof(int) * shm_size;
  if(id == 1) {
    printf("%d: get shm\n", id);
    get_shm(&mem1_id, &mem2_id, key1, key2);
    printf("%d: mem1_id = %d, mem2_id = %d\n", id, mem1_id, mem2_id);

    printf("%d: init arr\n", id);
    int* arr = init_arr();
    print_arr(arr, shm_size);
    
    printf("%d: write data\n", id);
    memcpy(mymem_ptr, arr, (size_t) shm_size_byte);
    int* mem_buf = mymem_ptr;
    mem_buf[0] = size;
    printf("%d: read data\n", id);
    int* arr2  = read_data_from_shm(othmem_ptr);
    print_arr(arr2, shm_size);

    printf("%d: clean mem\n", id);
    shmdt(othmem_ptr);
    shmdt(mymem_ptr);
    shmctl(mem1_id, IPC_RMID, NULL);
    printf("%d: end\n", id);
    
  }else {
    printf("%d: get shm\n", id);
    get_shm(&mem2_id, &mem1_id, key2, key1);
    printf("%d: mem1_id = %d, mem2_id = %d\n", id, mem1_id, mem2_id);

    printf("%d: read data\n", id);
    int* arr = read_data_from_shm(othmem_ptr);
    print_arr(arr, shm_size);
    printf("%d: write data\n", id);
    memcpy(mymem_ptr, arr, (size_t) shm_size_byte);

    printf("%d: clean mem\n", id);
    shmdt(othmem_ptr);
    shmdt(mymem_ptr);
    shmctl(mem2_id, IPC_RMID, NULL);
    printf("%d: end\n", id);
  }
}
