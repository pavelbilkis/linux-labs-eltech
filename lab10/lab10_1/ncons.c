#include <stdio.h>
#include "unistd.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include "sys/types.h"

int key = 100;
int shm_id = -1;
int buff_size = 2;
int end = 0;
struct sembuf decr1, incr0, incr2;
int my_sem = 1;
int oth_sem = 0;
int cur_index = 0;
int sem_size = 3;

char* get_buffer() {
  do {
    shm_id = shmget(key, (size_t) buff_size, IPC_EXCL);
  }while(shm_id == -1);
  return shmat(shm_id, NULL, 0);
}

int get_sem() {
  int sem_id = -1;
  do {
    sem_id = semget(key, sem_size, IPC_EXCL);
  } while(sem_id == -1);
  return sem_id;
}

void init_operations() {
  decr1.sem_num = (unsigned short) my_sem;
  decr1.sem_op = -1;
  decr1.sem_flg = 0;

  incr0.sem_num = (unsigned short) oth_sem;
  incr0.sem_op = 1;
  incr0.sem_flg = 0;

  incr2.sem_num = 2;
  incr2.sem_op = 1;
  incr2.sem_flg = 0;
}

void read_buffer(const char* buff, int sem_id) {
  printf("\n");
  semop(sem_id, &decr1, 1);

  char data = buff[cur_index];
  if(data == EOF) {
    end = 1;
    semop(sem_id, &incr2, 1);
    return;
  }
  printf("Read data: %c\n", data);
  int sem_prod_val = semctl(sem_id, 0, GETVAL, 0);
  if(sem_prod_val < buff_size)
    semop(sem_id, &incr0, 1);
  if(cur_index == 4) cur_index = 0;
  else cur_index++;
}

void work (char* buff, int sem_id) {
  while (!end) {
    read_buffer(buff, sem_id);
  }
}


int main() {
  char* buff = get_buffer();
  int sem_id = get_sem();

  init_operations();

  work(buff, sem_id);

  shmdt(buff);

  return 0;
}
