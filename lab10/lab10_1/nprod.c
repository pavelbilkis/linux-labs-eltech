#include <stdio.h>
#include "unistd.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include "sys/types.h"

int key = 100;
char* fname = "data.txt";
FILE* fin = NULL;
int shm_id;
int buff_size = 2;
int end = 0;
struct sembuf decr0, incr1, decr2;
int my_sem = 0;
int oth_sem = 1;
int cur_index = 0;
int sem_count = 3;

char* create_buffer(){
  shm_id = shmget(key, (size_t) buff_size, IPC_CREAT | 0666);
  return shmat(shm_id, NULL, 0);
}

int create_sem() {
  int sem_id = semget(key, sem_count, IPC_CREAT | 0666);
  semctl(sem_id, 0, SETVAL, buff_size);
  return sem_id;
}

void init_operations() {
  decr0.sem_num = (unsigned short) my_sem;
  decr0.sem_op = -1;
  decr0.sem_flg = 0;

  incr1.sem_num = (unsigned short) oth_sem;
  incr1.sem_op = 1;
  incr1.sem_flg = 0;

  decr2.sem_num = 2;
  decr2.sem_op = -1;
  decr2.sem_flg = 0;
}

void write_data(char* buff, int sem_id) {
  printf("\n");
  char data = (char) fgetc(fin);
  printf("File data = %c\n", data);
  semop(sem_id, &decr0, 1);
  int sem_val = semctl(sem_id, 0, GETVAL, 0);
  buff[cur_index] = data;
  semop(sem_id, &incr1, 1);
  sem_val = semctl(sem_id, 1, GETVAL, 0);
  if(cur_index == 4) cur_index = 0;
  else cur_index++;
  if(data == EOF) {
    printf("End of file\n");
    end = 1;
    semop(sem_id, &decr2, 1);
  }
}

void work(char* buff, int sem_id) {
  fin = fopen(fname, "r");
  while(!end) {
    write_data(buff, sem_id);
  }
  fclose(fin);
}

void delete_buff(char* buff) {
  struct shmid_ds arg_buff;
  do {
    shmctl(shm_id, IPC_STAT, &arg_buff);
  }while (arg_buff.shm_nattch == 2);
  shmdt(buff);
  shmctl(shm_id, IPC_RMID, NULL);
}

void delete_sem(int sem_id) {
  semctl(sem_id, 0, IPC_RMID, 0);
}

int main() {
  char* buff = create_buffer();
  int sem_id = create_sem();

  init_operations();

  work(buff, sem_id);

  delete_buff(buff);
  delete_sem(sem_id);
  return 0;
}
