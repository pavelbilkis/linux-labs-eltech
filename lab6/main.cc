#include <cstdint>
#include <iostream>
#include <string>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>

int16_t iterations;
timeval pbeg{0, 0}, pend{0, 0};
timeval cbeg{0, 0}, cend{0, 0};

void sig_handler (int sig) {
  if (sig == SIGALRM){
    pid_t current_pid = fork();
    gettimeofday(&cbeg, nullptr);
    
    if (current_pid < 0){
      // error handling
      std::cerr << "failed on fork()" << std::endl;
      exit(1);
    }else if (current_pid == 0){ 
      // child
      std::cout << "PID: " << getpid() << std::endl;
      
      time_t t;
      t = time(&t);
      std::cout << ctime(&t) << std::endl;
      exit(0);
    }else{
      // parrent
      
      wait(nullptr);
      gettimeofday(&cend, nullptr);
      gettimeofday(&pend, nullptr);

      timeval pdurat{0, 0}, cdurat{0, 0};
      timersub(&pend, &pbeg, &pdurat);
      timersub(&cend, &cbeg, &cdurat);

      std::cout << "child worked for " << cdurat.tv_sec << " sec "
		<< cdurat.tv_usec << " usec" << '\n';
      std::cout << "parent worked for " << pdurat.tv_sec << " sec "
		<< pdurat.tv_usec << " usec" << "\n\n";

      iterations--;
    }
    
  }else if(sig == SIGTSTP){
    // do nothing
    ;
  }else{
    printf("unknown signal");
  }
}

int main (int argc, char* argv[]) {
  if (argc < 3) {
    std::cerr << "usage: lab6 <interval in seconds> <iterations>"
	      << std::endl;
    return 0;
  }
  gettimeofday(&pbeg, nullptr);

  // set timer
  struct itimerval timer;
  // start immediately
  timer.it_value.tv_sec = 0;
  timer.it_value.tv_usec = 100;
  //ignore usec, parse seconds from args
  timer.it_interval.tv_usec = 0;
  timer.it_interval.tv_sec = std::stoi(argv[1]);

  // repeat counter
  iterations = std::stoi(argv[2]);

  // set timer
  setitimer(ITIMER_REAL, &timer, nullptr);

  // set action on SIGALARM
  struct sigaction psa;
  psa.sa_handler = sig_handler;
  sigaction(SIGALRM, &psa, NULL);
  sigaction(SIGTSTP, &psa, NULL);
  // sigaction(SIGTSTP, &psa, NULL);

  // until counter expires
  while (iterations > 0);
  exit(0);
}
