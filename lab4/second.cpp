#include <algorithm>
#include <iostream>
#include <fstream>
#include <memory>

struct LineWithFile {
  std::string line;
  std::ofstream* file;
};

void* write_line(void* args) {
  auto* arg = static_cast<LineWithFile*>(args);
  *(arg->file) << arg->line << std::endl;
  delete arg;
}

class Stripper {

private:
  static const bool ODD = true;
  static const bool EVEN = false;
  std::ifstream input_file;
  std::ofstream odd_file;
  std::ofstream even_file;

  pthread_t get_line_thread(bool parity) {
    //read line from input file
    std::string line;
    std::getline(input_file, line);

    //file arg structure
    auto* arg = new LineWithFile;
    arg->line = line;
    if (parity == ODD) {
      arg->file = &odd_file;
    } else {
      arg->file = &even_file;
    }

    //move writing in separate thread
    pthread_t thread;
    int create_status;
    if ((create_status = pthread_create(&thread, nullptr, write_line, arg)) != 0) {
      throw std::runtime_error("pthread_create error");
    }

    return thread;
  }

public:
  Stripper(std::string input_file_name, std::string outdir) {
    input_file.open(input_file_name.c_str(), std::ios::in);
    if (!input_file.is_open()) {
      throw std::runtime_error("Invalid input file");
    }

    if (outdir.back() != '/') {
      outdir.append("/");
    }

    odd_file.open((outdir + "odd").c_str(), std::ios::out);
    even_file.open(outdir + "even", std::ios::out);
    if (!odd_file.is_open() || !even_file.is_open()) {
      throw std::runtime_error("Can't open output files");
    }
  }

  ~Stripper() {
    input_file.close();
    odd_file.close();
    even_file.close();
  }

  void strip_lines() {
    size_t line_count = std::count(std::istreambuf_iterator<char>(input_file), std::istreambuf_iterator<char>(), '\n');
    input_file.seekg(0);
    int join_status;
   
    for (size_t i = 0; i < line_count - (line_count & 1); i += 2) {
      pthread_t odd_thread = get_line_thread(ODD);
      pthread_t even_thread  = get_line_thread(EVEN);
      
      if ((join_status = pthread_join(odd_thread, NULL)) != 0) {
	throw std::runtime_error("pthread_join error");
      }
      if ((join_status = pthread_join(even_thread, NULL)) != 0) {
	throw std::runtime_error("pthread_join error");
      }
    }

    if (line_count & 1 == 1) {
      pthread_t odd_thread = get_line_thread(ODD);
      if ((join_status = pthread_join(odd_thread, NULL)) != 0) {
	throw std::runtime_error("pthread_join error");
      }
    }
  }
};



int main(int argc, char* argv[]) {
  Stripper stripper(argv[1], argv[2]);
  stripper.strip_lines();
  return 0;
}
