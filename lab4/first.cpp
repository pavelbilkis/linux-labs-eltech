#include <cstdio>
#include <cstdint>
#include <iostream>
#include <vector>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sched.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

void* print_file_content(void* args) {
  struct sched_param init_param{.sched_priority = 19};
  pthread_setschedparam(pthread_self(), SCHED_FIFO, &init_param);

  int policy, status;
  struct sched_param param;
  if ((status = pthread_getschedparam(pthread_self(), &policy, &param)) != 0) {
    std::cerr << "error on pthread_getschedparam()" << std::endl;
    exit(1);
  }

  std::string policy_str;
  switch (policy) {
  case SCHED_FIFO:
    policy_str = "SCHED_FIFO";
    break;
  case SCHED_RR:
    policy_str = "SCHED_RR";
    break;
  case SCHED_OTHER:
    policy_str = "SCHED_OTHER";
    break;
  default:
    policy_str = "SCHED_UNKNOWN";
    break;
  }

  std::cout << "Child priority: " << param.sched_priority << "\n"
	    <<  "Sheduling policy: " << policy_str << std::endl;
	    

  int prioty_max = sched_get_priority_max(policy);
  int prioty_min = sched_get_priority_min(policy);

  if (prioty_max == -1 || prioty_min == -1) {
    perror("error on get priority");
    exit(1);
  }
  std::cout << "Maximum priority: " << prioty_max << "\n"
	    << "Minimum priority: " << prioty_min << std::endl;

  auto fd = static_cast<int*>(args)[0];
  auto size = static_cast<int*>(args)[1];
  
  int8_t* file_content = new int8_t[size + 1];
  ssize_t readed;
  if ((readed = read(fd, file_content, size)) == -1) {
    perror("error on read()");
    exit(1);
  }
  std::cout << file_content << std::endl;
  delete[] file_content;
#ifdef CLOSE_FILE
  close(fd);
#endif
  return nullptr;
}

int main(int argc, char* argv[]) {

  struct sched_param init_param{.sched_priority = 10};
  sched_setscheduler(getpid(), SCHED_FIFO, &init_param);

  if (argc < 2){
    printf("Please, enter a filename for input.\n");
    exit(1);
  }
  
  int fd_input = open(argv[1], O_RDONLY);
  if (fd_input == -1) {
    std::cerr << "Error on open()" << strerror(errno) << std::endl;
    return 1;
  }

  struct stat file_stat;
  if (fstat(fd_input, &file_stat) == -1) {
    perror("Error on fstat()");
    close(fd_input);
    return 1;
  }

  int args[] = {fd_input, static_cast<int>(file_stat.st_size)};
  int creation_status, join_status;
  pthread_t print_thread;
  if ((creation_status = pthread_create(&print_thread, nullptr,print_file_content, args)) != 0) {
    std::cerr << "pthread_create() error" << std::endl;
    close(fd_input);
    return 1;
  }

  if ((join_status = pthread_join(print_thread, NULL)) != 0) {
    std::cerr << "pthread_join() error" << std::endl;
    close(fd_input);
    return 1;
  }
  
  int policy, status, close_status;
  sched_param parent_param;
  if ((status = pthread_getschedparam(pthread_self(), &policy, &parent_param)) != 0) {
    std::cerr << "pthread_getschedparam() error" << std::endl;
    close(fd_input);
    return 0;
  }
  std::cout << "Parent priority: " << parent_param.sched_priority << std::endl;

  if ((close_status = close(fd_input)) == -1) {
    std::cout << "File have been closed by someone else!" << std::endl;
  } else {
    std::cout << "Parent thread closes file!" << std::endl;
  }
  return 0;
}
