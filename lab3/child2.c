#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int main (int argc, char **argv){
  if (argc < 3){
    printf("second child: too little arguments\n");
    exit(1);
  }
 
  char str[256] = {0};
  int fd = atoi(argv[1]);
  int delay = atoi(argv[2]);
  sleep(delay);
  sprintf(str, "SECOND CHILD: pid=%d, ppid=%d, delay=%d\n", getpid(), getppid(), delay);
  write(fd, str, 128);
}
