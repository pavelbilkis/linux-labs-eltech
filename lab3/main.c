#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main (int argc, char **argv){
  if (argc < 5){
    printf("Please, enter a filename for output and three delays.\n");
    exit(1);
  }
  int parent_delay = atoi(argv[2]);
  int child1_delay = atoi(argv[3]);
  // int child2_delay = atoi(argv[4]);
  
  int fd;
  if ((fd = open(argv[1], O_APPEND | O_CREAT | O_WRONLY)) == -1){
     printf("Error opening file: %s\n", strerror(errno));
  }
  
  char str[256] = {0};
  // create first child with fork
  pid_t pid = fork();
  
  if (pid != 0){ 
    // create second child with vfork
    pid = vfork();
    if (pid != 0){
      // parent process
      // delay
      sleep(parent_delay);
      sprintf(str, "PARENT: pid=%d, ppid=%d, delay=%d\n", getpid(), getppid(), parent_delay);
      write(fd, str, 128);
    }else{
      // second child
      char* sargv[3];
      sargv[0] = "/home/bilkis/docs/linux-labs/linux-labs-eltech/lab3/child2";
      sprintf(sargv[1], "%d", fd);
      sargv[2] = argv[4];
      if (execv("/home/bilkis/docs/linux-labs/linux-labs-eltech/lab3/child2", sargv) == -1){
	printf("Error while using execv: %s\n", strerror(errno));
      }
    }
  }else{
    // first child
    sleep(child1_delay);
    sprintf(str, "FIRST CHILD: pid=%d, ppid=%d, delay=%d\n", getpid(), getppid(), child1_delay);
    write(fd, str, 128);
  }
  
}
