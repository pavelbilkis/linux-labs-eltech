#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {
  // check args
  if (argc < 4) {
    std::cerr << "usage: task_1_main <in file> <in file> <out file>" << '\n';
    return 0;
  }

  // open output file
  std::ofstream out_file(argv[3], std::ios::out);
  if (!out_file.is_open()) {
    std::cerr << "invalid output file name" << std::endl;
    exit(1);
  }

  // open pipe
  int pipe_fd[2];
  if (pipe(pipe_fd) == -1) {
    perror("openning pipe errro");
    exit(1);
  }

  // make two subroutines

  const char* pipe_write = std::to_string(pipe_fd[1]).c_str();
  
  pid_t current_pid = vfork();
 
  if(current_pid == -1) {
    // error handling
    std::cerr << "Failed on fork()" << std::endl;
    exit(1);
  }else if(current_pid == 0) { // new process
    int status = execl("./child", "./child", argv[1], pipe_write, NULL);
    if (status == -1) {
      perror("subroutine execution error");
      exit(1);
    }
  }else{ // parrent
    current_pid = vfork();
    if (current_pid == 0){
      int status = execl("./child", "./child", argv[2], pipe_write, NULL);
      if (status == -1) {
	perror("subroutine execution error");
	exit(1);
      }
    }
  }

  // close unused write end
  close(pipe_fd[1]);
  
  // read the pipe
  char symbol;
  while (read(pipe_fd[0], &symbol, 1) > 0) {
    out_file << symbol;
  }

  // wait(nullptr);
  // wait(nullptr);
  close(pipe_fd[0]);
  
  out_file.close();
  return 0;
}
