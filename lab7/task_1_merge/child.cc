#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include <sys/types.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {
	// open input file
	std::ifstream in_file(argv[1], std::ios::in);
	if (!in_file.is_open()) {
		std::cerr << "invalid input file name" << std::endl;
		exit(1);
	}

	// obtain write end
	int pipe_fd = std::stoi(argv[2]);
	sleep(1);
	// transmit by line
	std::string line;
	while (std::getline(in_file, line)){
		line.push_back('\n');
		int wstatus = write(pipe_fd, line.data(), line.length());
		if (wstatus == -1) {
			perror("writing in pipe error");
			exit(1);
		}
	}

	close(pipe_fd);
	in_file.close();
	exit(0);
}
