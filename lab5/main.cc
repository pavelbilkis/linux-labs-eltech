#include <iostream>
#include <cstdint>
#include <cstdio>
#include <signal.h>

void sig_handler(int sig) {
  if (sig == SIGFPE){
    std::cerr << "I've received signal: SIGFPE" << std::endl;
    exit(1);
  }else if (sig == SIGSEGV){
    std::cerr << "I've received signal: SIGSEGV" << std::endl;
    exit(2);
  }
}

int main(int argc, char* argv[]) {
 
  if (argc < 2) {
    std::cerr << "use: lab5 [1 | 2]" << std::endl;
    return 0;
  }

  struct sigaction psa;
  psa.sa_handler = sig_handler;
  sigaction(SIGFPE, &psa, NULL);
  sigaction(SIGSEGV, &psa, NULL);

  uint8_t mode = atoi(argv[1]);
  
  if (mode == 1){
    uint8_t fail = 2 / 0;
  }else if (mode == 2){
    uint8_t* buffer = nullptr;
    uint8_t fail = buffer[5];
  }else{
    std::cerr << "Unknown mode" << std::endl;
  }
  
  return 0;
}
