#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <dirent.h>
#include "errno.h"

struct msgbuf {
  long mtype;
  char mtext[10];
};

int main(int argc, char* argv[])
{
  if (argc < 2){
    printf("usage: ./proc2 duration\n");
    exit(1);
  }
  int duration = atoi(argv[1]);
  int key = 10;
  int qid = msgget(key, IPC_EXCL);
  if(qid != -1){
    printf("qid = %d\n", qid);
    struct msgbuf msg;
    msg.mtype = 1;
    sprintf(msg.mtext, "%d", duration);
    int err = msgsnd(qid, &msg, sizeof(msg.mtext), IPC_NOWAIT);
    if (err == -1){
      printf("Send failed with error: %s \n", strerror(errno));
    }
  }
  else{
    printf("Main process is not running\n");
  }
  return 0;
}
