#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "errno.h"

struct msgbuf {
  long mtype;
  char mtext[10];
};

int main(int argc, char* argv[])
{
  if (argc < 2){
    printf("usage: ./main duration\n");
    exit(1);
  }
  int duration = atoi(argv[1]);
  int key = 10;
  int qid = msgget(key, IPC_CREAT | 0400 | 0200);
  if(qid != 1){
    printf("qid = %d\n", qid);
    struct msgbuf buff, msg;
    int counter = 1;
    int err = 0;
    while (counter <= duration){
      err = msgrcv(qid, &buff, sizeof(msg.mtext), 1, IPC_NOWAIT);//ERROR here
      if(err != -1) {
	printf("Msg has been received\n");
	duration = atoi(buff.mtext);
	printf("New time = %d\n", duration);
      }
      else {
	printf("Main process is sleeping...%d\n", counter);
	sleep(1);
	counter++;
      }
    }
    printf("Delete queue\n");
    msgctl(qid, IPC_RMID, NULL);
  }
  else {
    abort();
  }
  return 0;
}
