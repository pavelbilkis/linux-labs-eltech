#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>

//TODO: delete queue
//Process ids = 1 2 3

struct msg {
  long mtype;
  int pnumber;//can be as sender as receiver
  int qid;
  int time;
};

int pnumber_m;
int pnumber_1;
int pnumber_2;
int qid = -1;
int own_gid = -1;
int master = 0;
int read = 0;
int other_requests_count = 0;

int request_type = 0;
int response_type = 100;

struct msg *response_for_me1 = NULL;
struct msg *response_for_me2 = NULL;

int read_time = 0.0;

int koef(int sender, int receiver) {
  return 2 * sender + 3 * receiver;
}

int send_msg(int type, int pnumber, int time) {
  struct msg message;
  message.mtype = type;
  message.pnumber = pnumber;
  message.qid = qid;
  message.time = time;
     /* printf("\nmessage.mtype = %d\n", message.mtype); */
     /* printf("message.pnumber = %d\n", message.pnumber); */
     /* printf("message.qid = %d\n", message.qid); */
     /* printf("message.time = %f\n", message.time); */
  int msgsize = sizeof(message.qid) + sizeof(message.pnumber) + sizeof(message.time);
  //printf("msgsize = %d\n", msgsize);
  int err = msgsnd(qid, &message, msgsize, 0);
  return err;
}

void receive_request_and_send_response(struct msg **request, int sender) {
  //printf("%d: receive_request_and_send_response\n", pnumber_m);
  struct msg *req = (struct msg*) malloc(sizeof(struct msg));
  int msgsize = sizeof(req->pnumber) + sizeof(req->qid) + sizeof(req->time);//16;
  int err = msgrcv(qid, req, msgsize, request_type + koef(sender, pnumber_m), IPC_NOWAIT);
  if(err == -1) {
    //printf("%d: Error to receive request: sender = %d, err = %d\n", pnumber_m, sender, errno);
    return;
  }
  printf("%d: request is succenfully received, sender = %d\n", pnumber_m, sender);
  other_requests_count++;

  int resp_time = (int) time(NULL);
  if(!read) {
    if(req->time < read_time) {
      send_msg(response_type + koef(pnumber_m, sender), pnumber_m, resp_time);
    }
    else *request = req;
  }
  else send_msg(response_type + koef(pnumber_m, sender), pnumber_m, resp_time);

}

void receive_responses() {
  //printf("%d: receive responses\n", pnumber_m);
  struct msg *resp1 = (struct msg*) malloc(sizeof(struct msg));
  struct msg *resp2 = (struct msg*) malloc(sizeof(struct msg));
  int msgsize1 = sizeof(resp1->pnumber) + sizeof(resp1->qid) + sizeof(resp1->time);
  int msgsize2 = sizeof(resp2->pnumber) + sizeof(resp2->qid) + sizeof(resp2->time);
  int err1 = msgrcv(qid, resp1, msgsize1, response_type + koef(pnumber_1, pnumber_m), IPC_NOWAIT);
  int err2 = msgrcv(qid, resp2, msgsize2, response_type + koef(pnumber_2, pnumber_m), IPC_NOWAIT);

  if(err1 != -1) {
    printf("%d: response is succesfully received, sender = %d\n", pnumber_m, pnumber_1);
    response_for_me1 = resp1;
  }
  //else printf("%d: error to receive response, sender = %d, errno = %d\n", pnumber_m, pnumber_1, errno);
  if(err2 != -1) {
    printf("%d: response is succesfully received, sender = %d\n", pnumber_m, pnumber_2);
    response_for_me2 = resp2;
  }
  //else printf("%d: error to receive response, sender = %d, errno = %d\n", pnumber_m, pnumber_2, errno);
}

void send_requests() {
  printf("%d: send requests\n", pnumber_m);
  int err1 = send_msg(request_type + koef(pnumber_m, pnumber_1), pnumber_m, read_time);
  int err2 = send_msg(request_type + koef(pnumber_m, pnumber_2), pnumber_m, read_time);
  if(err1 == 0) printf("%d: req1 has successfully sent\n", pnumber_m);
  else printf("%d: req1 has not successfully sent, errno = % d\n", pnumber_m, errno);
  if(err2 == 0) printf("%d: req2 has successfully sent\n", pnumber_m);
  else printf("%d: req2 has not successfully sent, errno = % d\n", pnumber_m, errno);
}

int main(int argc, char* argv[])
{
  if (argc < 4){
    printf("usage: ./main pnumber_m pnumber_1 pnumber_2\n");
    exit(1);
  }
  pnumber_m = atoi(argv[1]);
  pnumber_1 = atoi(argv[2]);
  pnumber_2 = atoi(argv[3]);
  printf("pnumber_m = %d\n", pnumber_m);
  printf("pnumber_1 = %d\n", pnumber_1);
  printf("pnumber_2 = %d\n", pnumber_2);

  // shared queue
  int key = 110;
  qid = msgget(key, 0);
  if(qid == -1){
    printf("%s\n", strerror(errno));
    qid = msgget(key, IPC_CREAT | 0400 | 0200);
    master = 1;
    printf("Creating a new msg queue. key = %d, qid = %d \n", key, qid);
  }else{
    printf("Opened an existing msg queue. key = %d, qid = %d \n", key, qid);
  }

  if(qid == -1) {
    printf("%d: error creating queue\n", pnumber_m);
    exit(1);
  }

  
  
  printf("master = %d\n", master);
  char* fname = "file.txt";
  read_time = (int) time(NULL);
  printf("%d:read time = %d\n", pnumber_m, read_time);
  send_requests();

  struct msg *other_request1 = NULL;
  struct msg *other_request2 = NULL;

  while(!read) {
    receive_request_and_send_response(&other_request1, pnumber_1);
    receive_request_and_send_response(&other_request2, pnumber_2);
    receive_responses();
    if(response_for_me1 && response_for_me2) {
      FILE* input_f = fopen(fname, "r");
      size_t size = 0;
      char* line = NULL;
      printf("%d: time1 = %d, time2 = %d\n", pnumber_m, response_for_me1->time, response_for_me2->time);
      while(getline(&line, &size, input_f) != -1) printf("%s", line);
      fclose(input_f);
      read = 1;
      while(other_requests_count != 2) {
	receive_request_and_send_response(&other_request1, pnumber_1);
	receive_request_and_send_response(&other_request2, pnumber_2);
      }
    }
    sleep(1);
  }
  if(other_request1) send_msg(response_type + koef(pnumber_m, pnumber_1), pnumber_m, (int) time(NULL));
  if(other_request2) send_msg(response_type + koef(pnumber_m, pnumber_2), pnumber_m, (int) time(NULL));
  //    if(master){
  //        struct msqid_ds buf;
  //        msgctl(qid, IPC_STAT, &buf);
  //        while(buf.msg_qnum != 0) {
  //            sleep(1);
  //            msgctl(qid, IPC_STAT, &buf);
  //        }
  //        msgctl(qid, IPC_RMID, NULL);
  //    }
  return 0;
}
